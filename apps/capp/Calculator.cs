using System;
using System.Linq;
using System.Collections.Generic;

namespace capp
{
    public class Calculator
    {
        public int Sum(string inputs = null)
        {
            if (inputs == null)
                return 0;
            
            var delimiters = new List<char>() {',', '\n'};
            var indexOfDoubleSlash = inputs.IndexOf("//");
            if ( indexOfDoubleSlash > -1) {
                delimiters.Add(inputs[indexOfDoubleSlash+1+1]); // "//;\n1;2"
            }

            // check for comma to make sure that there is more than one values passed
            var numbersToAdd = inputs.Split(delimiters.ToArray(), StringSplitOptions.RemoveEmptyEntries); // 1\n2,3\n => 1,2,3
            var sum = 0;
            foreach(var num in numbersToAdd)
            {
                var curNum = 0;
                if(int.TryParse(num,out curNum))
                {
                    sum+= curNum;
                }
            }
            return sum;
        }
    }
}