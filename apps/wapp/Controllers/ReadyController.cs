using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace wapp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReadyController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Project", "Setup", "On", "Gitpod", "For", "Pair", "Programming", "Session", "."
        };

        private readonly ILogger<ReadyController> _logger;

        public ReadyController(ILogger<ReadyController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<string> Get() => Summaries;
    }
}
