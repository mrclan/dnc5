using System;
using Xunit;
using capp;

namespace capp.Tests
{
    public class CalculatorTests
    {
        [Fact]
        // Given the user input is empty when calculating the sum then it should return zero.
        public void Given_The_User_Input_Is_Empty_Should_Return_Zero()
        {
            // Arrange
            var calc = new Calculator();
            var expect = 0;

            // Act
            var result = calc.Sum();

            // Assert
            Assert.True(expect == result, $"Expected 0, but got {result}");
        }

        [Fact]
        // Given the user input is one number when calculating the sum then it should return the same number. (example "3" should equal 3)
        public void Given_The_User_Input_Is_One_Number_Should_Return_Same_Number()
        {
            // Arrange
            var calc = new Calculator();
            var expect = 2;

            // Act
            var result = calc.Sum("2");

            // Assert
            Assert.True(expect == result, $"Expected 2, but got {result}");
        }
        
        [Fact]
        // Given the user input is two numbers when calculating the sum then it should return the sum of those numbers. (example "1,2" should equal 3)
        public void Given_The_User_Input_Is_Two_Numbers_Should_Return_The_Sum_Of_Those_Numbers()
        {
            // Arrange
            var calc = new Calculator();
            var expect = 3;

            // Act
            var result = calc.Sum("1,2");

            // Assert
            Assert.True(expect == result, $"Expected 3, but got {result}");
        }

        [Fact]
        // Given the user input is an unknown amount of numbers when calculating the sum then it should return the sum of all the numbers
        public void Given_The_Input_Is_Unknown_Amount_Of_Numbers_Return_Sum_Of_All_Of_The_Numbers()
        {
            var calc = new Calculator();
            var expect = 3;

            // Act
            var result = calc.Sum("0,0,0,1,0,1,0,1");

            // Assert
            Assert.True(expect == result, $"Expected 3, but got {result}");
        }
        
        [Fact]
        // Given the user input is multiple numbers with new line and comma delimiters when calculating the sum then it should return the sum of all the numbers. (example "1\n2,3" should equal 6)
        public void Given_The_Input_Is_Multiple_Numbers_With_New_Line_And_Comma_Delimiters_Should_Return_Sum_Of_All_Of_The_Numbers()
        {
            var calc = new Calculator();
            var expect = 6;

            // Act
            var result = calc.Sum("1\n2,3");

            // Assert
            Assert.True(expect == result, $"Expected 6, but got {result}");
        }

        [Fact]
        // Given the user input is multiple numbers with a custom single-character delimiter when calculating the sum then it should return the sum of all the numbers. (example “//;\n1;2” should return 3)
        public void Given_The_Input_Is_Multiple_Numbers_With_Custom_Single_Line_Delimiter_Should_Return_Sum_Of_All_Of_The_Numbers()
        {
            var calc = new Calculator();
            var expect = 3;

            // Act
            var result = calc.Sum("//;\n1;2");

            // Assert
            Assert.True(expect == result, $"Expected 3, but got {result}");
        }


        
        // Given the user input contains one negative number when calculating the sum then it should throw an exception "negatives not allowed: x" (where x is the negative number).
        // Given the user input contains multiple negative numbers mixed with positive numbers when calculating the sum then it should throw an exception "negatives not allowed: x, y, z" (where x, y, z are only the negative numbers).
        // Given the user input contains numbers larger than 1000 when calculating the sum it should only sum the numbers less than 1001. (example 2 + 1001 = 2)
        // Given the user input is multiple numbers with a custom multi-character delimiter when calculating the sum then it should return the sum of all the numbers. (example: “//[]\n12***3” should return 6)
        // Given the user input is multiple numbers with multiple custom delimiters when calculating the sum then it should return the sum of all the numbers. (example “//[][%]\n12%3” should return 6)
    }
}