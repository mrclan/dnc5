using System;
using Xunit;
using Moq;
using Microsoft.Extensions.Logging;
using wapp.Controllers;
using System.Collections.Generic;
using System.Linq;


namespace wapp.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void ReadyController_Get_ShouldReturn_8Words()
        {
            var mockedLogger = new Mock<ILogger<ReadyController>>();
            var logger = mockedLogger.Object;

            var readyController = new ReadyController(logger);

            var getResult = readyController.Get();

            Assert.True(getResult.Count() == 8, $"Expecting 8 words, got {getResult.Count()}.");
        }

        
        [Fact]
        public void ReadyController_Get_ShouldNotReturn_Period()
        {
            var mockedLogger = new Mock<ILogger<ReadyController>>();
            var logger = mockedLogger.Object;

            var readyController = new ReadyController(logger);

            var getResult = readyController.Get();

            Assert.False(getResult.Any(w=> w == "."), $"Expected to not return \".\" but was found.");
        }

        [Fact]
        public void ReadyController_Get_ShouldContain_PairSessionWord()
        {
            var mockedLogger = new Mock<ILogger<ReadyController>>();
            var logger = mockedLogger.Object;

            var readyController = new ReadyController(logger);

            var getResult = readyController.Get();

            Assert.True(getResult.Any(w=> w == "Pair") && getResult.Any(w=> w == "Session"), $"Expected the words PAIR and SESSION to be present.");
        }
    }
}
